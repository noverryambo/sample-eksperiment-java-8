package ex.samplejava8.repository;

import ex.samplejava8.model.MasterAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */
public interface MasterAccountRepository extends JpaRepository<MasterAccount, Long> {
    Optional<MasterAccount> findByUsernameOrEmail(String username, String email);
    Boolean existsByUsername(String username);
}
