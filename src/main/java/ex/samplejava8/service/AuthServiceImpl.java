package ex.samplejava8.service;

import ex.samplejava8.dto.RequestLogin;
import ex.samplejava8.dto.RequestRegister;
import ex.samplejava8.dto.ResponseLogin;
import ex.samplejava8.filter.JwtToken;
import ex.samplejava8.model.MasterAccount;
import ex.samplejava8.repository.MasterAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@Service
@Slf4j
public class AuthServiceImpl implements AuthService{

    @Autowired
    private MasterAccountRepository masterAccountRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    @Transactional
    public ResponseLogin doRegister(RequestRegister requestRegister) {

        String currentUser = doGetUsername();
        Optional<MasterAccount> reference = masterAccountRepository.findByUsernameOrEmail(requestRegister.getUsername(), requestRegister.getUsername());
        if (reference.isPresent()) {
            return ResponseLogin.builder()
                    .responseCode("01")
                    .responseMessage("User Already Register")
                    .build();
        }else{
            MasterAccount user = new MasterAccount();
            user.setCreatedBy(currentUser);
            user.setUsername(requestRegister.getUsername());
            user.setPassword(BCrypt.hashpw(requestRegister.getPassword(), BCrypt.gensalt()));
            user.setEmail(requestRegister.getEmail());
            user.setFullName(requestRegister.getFullName());
            masterAccountRepository.save(user);

            return ResponseLogin.builder()
                    .responseCode("00")
                    .responseMessage("Register Success")
                    .data(user)
                    .build();
        }
    }

    @Override
    @Transactional
    public ResponseLogin doLogin(RequestLogin requestLogin) {

        String current = doGetUsername();
        Optional<MasterAccount> currentUser = masterAccountRepository.findByUsernameOrEmail(requestLogin.getUsername(), requestLogin.getUsername());
        if (!currentUser.isPresent()){
            return ResponseLogin.builder()
                    .responseCode("01")
                    .responseMessage("USER NOT FOUND")
                    .build();
        }
        MasterAccount user = currentUser.get();

        if (user.getPassword() != null && BCrypt.checkpw(requestLogin.getPassword(), user.getPassword())){
            UserDetails userDetails = new User(user.getUsername(), user.getPassword(), new ArrayList<>());
           String token = jwtToken.generateToken(userDetails);
            user.setToken(token);
            user.setUpdatedBy(current);

            return ResponseLogin.builder()
                    .responseCode("00")
                    .responseMessage("LOGIN SUCCESS")
                    .data(user)
                    .token(token)
                    .build();
        }else {
            return ResponseLogin.builder()
                    .responseCode("01")
                    .responseMessage("LOGIN FAILED")
                    .build();
        }

    }

    @Override
    public boolean existByUsername(String username) {
        Boolean isEksist = masterAccountRepository.existsByUsername(username);
        return isEksist;
    }

    private String doGetUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String user = authentication.getName();
        return user;
    }
}
