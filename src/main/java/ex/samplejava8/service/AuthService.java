package ex.samplejava8.service;

import ex.samplejava8.dto.RequestLogin;
import ex.samplejava8.dto.RequestRegister;
import ex.samplejava8.dto.ResponseLogin;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

public interface AuthService {

    ResponseLogin doRegister(RequestRegister requestRegister);
    ResponseLogin doLogin(RequestLogin requestLogin);
    boolean existByUsername(String username);

}
