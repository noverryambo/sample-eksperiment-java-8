package ex.samplejava8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleJava8Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleJava8Application.class, args);
	}

}
