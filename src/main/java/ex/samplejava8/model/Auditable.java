package ex.samplejava8.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> {

    @CreatedBy
    protected U createdBy;

    @CreatedDate
    @Temporal(TIMESTAMP)
    @JsonFormat(timezone = "Asia/Jakarta")
    protected Date createdDate;

    @LastModifiedBy
    protected U updatedBy;

    @LastModifiedDate
    @Temporal(TIMESTAMP)
    @JsonFormat(timezone = "Asia/Jakarta")
    protected Date updatedDate;

    @PrePersist
    protected void onCreate(){
        setCreatedDate(new Date());
    }

    @PreUpdate
    protected void onUpdate(){
        setUpdatedDate(new Date());
    }

}
