package ex.samplejava8.controller;

import ex.samplejava8.dto.RequestLogin;
import ex.samplejava8.dto.RequestRegister;
import ex.samplejava8.dto.ResponseLogin;
import ex.samplejava8.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthService authService;

    @PreAuthorize("permitAll()")
    @PostMapping(value = "do-register")
    public ResponseEntity<?> doRegister(@RequestBody RequestRegister param){
        ResponseLogin response = authService.doRegister(param);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/do-login")
    public ResponseEntity<?> doLogin(@RequestBody RequestLogin login){
        ResponseLogin doLogin = authService.doLogin(login);
        return new ResponseEntity<>(doLogin, HttpStatus.OK);
    }
}
