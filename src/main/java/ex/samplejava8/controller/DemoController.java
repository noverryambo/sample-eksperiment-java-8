package ex.samplejava8.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author "Noverry Ambo"
 * @start 3/18/2024
 */

@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/check-auth")
    public ResponseEntity<?> checkAuth(){
        String check = "ini adalah api menggunakan security";
        return new ResponseEntity<>(check, HttpStatus.OK);
    }
}
