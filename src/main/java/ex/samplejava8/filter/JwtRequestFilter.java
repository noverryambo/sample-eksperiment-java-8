package ex.samplejava8.filter;

import ex.samplejava8.exception.InvalidTokenException;
import ex.samplejava8.exception.UnauthorizedException;
import ex.samplejava8.model.MasterAccount;
import ex.samplejava8.repository.MasterAccountRepository;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private MasterAccountRepository masterAccountRepository;

    @Autowired
    private JwtToken jwtToken;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String requestHeaderToken = request.getHeader("Authorization");

        String username = null;
        String tokenJwt = null;

        if (requestHeaderToken != null){
//            tokenJwt = requestHeaderToken.split(" ")[1].trim();;
            tokenJwt = requestHeaderToken.substring(7);

            try{
                username = jwtToken.getUsernameaFromToken(tokenJwt);
            }catch (IllegalArgumentException e){
                throw new InvalidTokenException("Unable to get Jwt Token");
            }catch (ExpiredJwtException ex){
                throw new InvalidTokenException("Token has expired");
            }
        }


        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null){
            MasterAccount user = new MasterAccount();
            user.setUsername(username);

            user = masterAccountRepository.findByUsernameOrEmail(username, username)
                    .orElseThrow(() -> new UnauthorizedException("Username Not Found"));

            if (jwtToken.validationToken(tokenJwt, user)) {
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }

        filterChain.doFilter(request, response);


    }
}
