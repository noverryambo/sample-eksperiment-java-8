package ex.samplejava8.exception;

/**
 * @author "Noverry Ambo"
 * @start 3/23/2024
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(String message) {
        super(message);
    }


}
