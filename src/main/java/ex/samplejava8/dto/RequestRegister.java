package ex.samplejava8.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestRegister {

    private String username;
    private String fullName;
    private String password;
    private String email;
}
