package ex.samplejava8.config;

import org.springframework.security.core.Authentication;

/**
 * @author "Noverry Ambo"
 * @start 3/17/2024
 */
public interface IAuthenticationFacade {
    Authentication getAuthentication();
}
